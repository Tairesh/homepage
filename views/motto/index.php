<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MottoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mottos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="motto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Motto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'text',

            [
                'format' => 'raw',
                'label' => 'Actions',
                'value' => function($model, $key, $index, $column) {
                    return Html::a('[Update]', ['update', 'id' => $model->id]) . ' ' .
                        Html::a('[Delete]', ['delete', 'id' => $model->id], [
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                        ]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
