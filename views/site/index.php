<?php

use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $posts app\models\Post[] */
/* @var $pagination yii\data\Pagination */

$this->title = Yii::$app->name;
$this->registerMetaTag(['name' => 'description', 'content' => "Личный блог Ильи Агафонова"], "description");

?>
<?php if (!Yii::$app->user->isGuest): ?>
    <p>
        <a class="adminlink" href="/post/create">[ Новый пост ]</a>
    </p>
<?php endif ?>
<?php foreach ($posts as $i => $post): ?>
    <?= $this->render('/post/_preview', ['post' => $post]) ?>
<?php endforeach ?>
<?= LinkPager::widget(['pagination' => $pagination]) ?>