<?php

use app\models\Post;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $tag app\models\Tag */
/* @var $posts app\models\Post[] */
/* @var $pagination yii\data\Pagination */

$this->title = Yii::$app->name . ' / ' . $tag->name;
$this->registerMetaTag(['name' => 'description', 'content' => "Публикации в категории {$tag->name}"], "description");
$this->registerMetaTag(['name' => 'keywords', 'content' => "{$tag->name}"], "keywords");

?>
<h1 class="page-title">Category: <span class="tag active"><?= $tag->name ?></span></h1>
<?php foreach ($posts as $i => $post): ?>
    <?= $this->render('/post/_preview', ['post' => $post]) ?>
<?php endforeach ?>
<?= LinkPager::widget(['pagination' => $pagination]) ?>