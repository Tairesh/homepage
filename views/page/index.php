<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            'label',
            'slug',
//            'content:ntext',
            'isActive:boolean',
            'inMenu:boolean',
            [
                'format' => 'raw',
                'label' => 'Actions',
                'value' => function($model, $key, $index, $column) {
                    return Html::a('[Update]', ['update', 'slug' => $model->slug]) . ' ' .
                    Html::a('[Delete]', ['delete', 'slug' => $model->slug], [
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                        ]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
