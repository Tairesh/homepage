<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = Yii::$app->name . ' / ' . $model->title;

?>
<?= $model->content ?>
<?php if(!Yii::$app->user->isGuest): ?>
    <a class="adminlink" href="/page/update?slug=<?=$model->slug?>">[ Редактировать ]</a>
<?php endif ?>
