<?php

/* @var $this \yii\web\View */

use app\widgets\Motto;
use app\widgets\TopMenu;

?>
<header>
    <div class="container">
        <a class="logo" href="/"><img class="lemon" src="/img/lemon16.png" alt="🍋"> tairesh.xyz <span class="lightgreen">~</span>&gt;</a>
        <?= Motto::widget() ?>
        <?= TopMenu::widget() ?>
    </div>
</header>