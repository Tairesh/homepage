<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\TagsList;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('_head') ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <?=$this->render('_header')?>
    <section class="container" role="main">
        <section class="col leftcol">
            <?= $content ?>
        </section>
        <section class="col rightcol">
            <div class="card">
                <h5 class="card-header">About me</h5>
                <div class="card-body" itemscope itemtype="http://schema.org/Person">
                    <p>This site is a personal blog of <span itemprop="name">Ilya Agafonov</span></p>
                    <p>Email: <a href="mailto:tairesh.rus+fromsite@gmail.com" itemprop="email">tairesh.rus@gmail.com</a></p>
                    <p>Telegram: <a href="https://t.me/Tairesh">@Tairesh</a></p>
                    <p>Source code of the site available on <a href="https://gitlab.com/Tairesh/homepage">GitLab</a></p>
                </div>
            </div>
            <div class="card">
                <h5 class="card-header">Blog categories</h5>
                <div class="card-body">
                    <?= TagsList::widget() ?>
                </div>
            </div>
            <div class="card">
                <h5 class="card-header">Donate</h5>
                <div class="card-body">
                    <p>If you wanna support me for keeping developing <a href="https://t.me/AshleyBot">@AshleyBot</a> or <a href="https://tairesh.itch.io/necromanzer-7drl">NecromanZer</a> or something else you can use these methods:</p>
                    <ul>
                        <li>PayPal — <a href="https://paypal.me/tairesh">paypal.me/tairesh</a></li>
                        <li>Sberbank Online — you can send money directly by phone number +79826151298</li>
                        <li>Yandex Money and other bank`s cards — <a href="https://money.yandex.ru/to/41001207149342">https://money.yandex.ru/to/41001207149342</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </section>
    <?=$this->render('_footer')?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
