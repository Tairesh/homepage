<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#292929">
<meta name="author" content="Ilya Agafonov">
<link rel="shortcut icon" href="/favicon.ico">
<link rel="icon" type="image/png" sizes="32x32" href="/img/lemon32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/img/lemon16.png">
<link rel="icon" type="image/png" sizes="128x128" href="/img/lemon128.png">
<link rel="apple-touch-icon" sizes="180x180" href="/img/lemon180.png">
<meta name="msapplication-TileImage" content="<?= Url::base(true) ?>/img/lemon512.png">
<meta property="twitter:image" content="<?= Url::base(true) ?>/img/lemon128.png">
<meta property="og:type" content="website">
<meta property="og:url" content="<?= Url::current(['lg'=>null], true) ?>">
<meta property="og:image" content="<?= Url::base(true) ?>/img/lemon128.png">
<meta property="og:image:width" content="128">
<meta property="og:image:height" content="128">
<link rel="alternate" type="application/rss+xml" title="RSS feed" href="/rss.xml" />

<?= Html::csrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
