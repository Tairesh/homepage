<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->render('_head') ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper wrapper404">
    <?=$this->render('_header')?>
    <section class="container" role="main">
        <section class="col fullcol text-center">
            <?= $content ?>
        </section>
    </section>
    <?=$this->render('_footer')?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
