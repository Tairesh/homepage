<?php

use app\helpers\DateHelper;
use app\widgets\TagsList;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $post app\models\Post */

$dateHuman = DateHelper::getTimeAgo($post->dateCreated);
$dateOnServer = DateHelper::dateInClientTimezone('d.m.Y H:i', $post->dateCreated);

?>
<article class="post">
    <h2 class="post-title">
        <?= Html::a($post->title, "/posts/{$post->slug}") ?>
    </h2>
    <div class="post-body">
        <?= $post->preview ?>
    </div>
    <div class="post-footer">
        <?= Html::a("<span class='date' data-timestamp='{$post->dateCreated}'>{$dateOnServer}</span> ({$dateHuman})", "/posts/{$post->slug}", ['class' => 'post-published']) ?>
        <?php if (!Yii::$app->user->isGuest): ?>
            <a class="adminlink" href="/post/update?id=<?=$post->id?>">[ EDIT ]</a>
        <?php endif ?>
        <?= TagsList::widget(['post' => $post]) ?>
    </div>
</article>