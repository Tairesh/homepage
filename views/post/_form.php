<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Tag;
use app\models\Post;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

$model->setTags = ArrayHelper::getColumn($model->tags,'id');

?>

<div class="post-form">

    <pre>
        Examples:<br>
        <?= Html::encode('<div class="youtube-player" data-id="ZH4YSF-i5dY"></div>') ?><br>
        <?= Html::encode('<div class="youtube-player format4x3" data-id="-0k6_ePGYHQ"></div>') ?><br>
        <?= Html::encode(Post::CUT) ?>
    </pre>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hidden')->checkbox() ?>

    <?= $form->field($model, 'dateCreated')->textInput() ?>

    <?= $form->field($model, 'setTags', ['template' => "{label} ".Html::a("[ Добавить тег ]", ['/tag/create'], ['class' => 'adminlink'])."\n{input}\n{hint}\n{error}"])->widget(Select2::classname(), [
            'language' => 'ru-RU',
            'data' => ArrayHelper::map(Tag::find()->orderBy(['name' => 'ASC'])->all(),'id','name'),
            'options' => ['multiple' => 'multiple'],
            'pluginOptions' => [
                'allowClear' => true,
                'buttonContainer'=>'<div class="form-group" style="position:relative;" />',
                'checkboxName' => 'Post[setTags]'
            ],
        ]);
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
