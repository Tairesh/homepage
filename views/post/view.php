<?php

use app\helpers\DateHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = Yii::$app->name . ' / ' . $model->title;
$this->registerMetaTag(['name' => 'description', 'content' => $model->preview], "description");

$dateHuman = DateHelper::getTimeAgo($model->dateCreated);
$dateOnServer = DateHelper::dateInClientTimezone('d.m.Y H:i', $model->dateCreated);

?>
<article class="post">
    <h2 class="post-title"><?= Html::encode($model->title) ?></h2>
    <div class="post-body">
        <?= $model->content ?>
    </div>
    <div class="post-footer">
        <span class="post-published">Published <span class="date" data-timestamp="<?=$model->dateCreated?>"><?= $dateOnServer ?></span> (<?= $dateHuman ?>)</span>
        <?php if (!Yii::$app->user->isGuest): ?>
            <a class="adminlink" href="/post/update?id=<?=$model->id?>">[ EDIT ]</a>
        <?php endif ?>
        <ul class="post-tags tagslist">
            <?php foreach ($model->tags as $tag): ?>
                <li>
                    <?= Html::a($tag->name, "/tags/{$tag->name}") ?>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</article>