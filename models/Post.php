<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property integer $dateCreated
 * @property boolean $hidden
 * 
 * @property Tag[] $tags
 * @property string $preview
 */
class Post extends ActiveRecord
{

    const CUT = '<!--cut-->';

    /**
     * @var array|string
     */
    public $setTags = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'content'], 'required'],
            [['content'], 'string'],
            [['dateCreated'], 'integer'],
            [['hidden'], 'boolean'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['setTags'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'dateCreated',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'content' => 'Content',
            'dateCreated' => 'Date Created',
            'hidden' => 'Hidden',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$this->setTags) {
            $this->setTags = [];
        }
        $this->setTags = array_map('intval', $this->setTags);
        $savedTags = [];
        foreach ($this->tags as $tag) {
            if (in_array(intval($tag->id), $this->setTags)) {
                $savedTags[] = intval($tag->id);
            } else {
                $this->unlink('tags', $tag, true);
                $tag->calcRating();
            }
        }
        foreach ($this->setTags as $tagId) {
            if (!in_array($tagId, $savedTags)) {
                $tag = Tag::findOne($tagId);
                $this->link('tags', $tag);
                $tag->calcRating();
            }
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tagId'])
            ->viaTable('posts2tags', ['postId' => 'id'])
            ->orderBy(['name' => 'ASC']);
    }

    public static function findByTag(Tag $tag)
    {
        return self::find()
            ->leftJoin('posts2tags', '"posts2tags"."postId"="posts"."id"')
            ->leftJoin('tags', '"tags"."id"="posts2tags"."tagId"')
            ->where(['tags.id' => $tag->id]);
    }

    public function getPreview()
    {
        $cut_pos = strpos($this->content, self::CUT);
        if ($cut_pos !== false) {
            return substr($this->content, 0, $cut_pos);
        } else {
            return $this->content;
        }
    }
}

