<?php

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "mottos".
 *
 * @property int $id
 * @property string $text
 */
class Motto extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mottos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
        ];
    }

    public static function randomText() : string
    {
        try {
            $tableName = self::tableName();
            return Yii::$app->db->createCommand("
                    SELECT text FROM {$tableName} WHERE id = (
                        SELECT round(random() * ((SELECT max(id) FROM mottos) - 1))+1
                    );
                ")->queryScalar();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
