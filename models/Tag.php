<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 * @property integer $rating
 *
 * @property Post[] $posts
 */
class Tag extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['rating'], 'default', 'value' => 0],
            [['rating'], 'integer'],
            [['name'], 'string', 'max' => 127],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rating' => 'Rating',
        ];
    }
    
    public function beforeSave($insert)
    {
        $this->name = mb_strtolower($this->name);
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, ['id' => 'postId'])->viaTable('posts2tags', ['tagId' => 'id']);
    }
    
    public function calcRating($save = true)
    {
        $this->rating = $this->getPosts()->count();
        if ($save) {
            $this->save();
        }
    }

    public static function findByName($name)
    {
	    return self::find()->where(['name' => $name])->one();
    }
}
