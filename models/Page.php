<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $title
 * @property string|null $label
 * @property string $slug
 * @property string $content
 * @property bool|null $isActive
 * @property bool|null $inMenu
 */
class Page extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'content'], 'required'],
            [['content'], 'string'],
            [['isActive', 'inMenu'], 'boolean'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['label'], 'string', 'max' => 127],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'label' => 'Label',
            'slug' => 'Slug',
            'content' => 'Content',
            'isActive' => 'Is Active',
            'inMenu' => 'In Menu',
        ];
    }
}
