<?php

namespace app\controllers;

use app\models\Tag;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Page;
use app\models\Post;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'error',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $query = Post::find()->where(['hidden' => false]);
        $countQuery = clone $query;
        
        $pagination = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 30,
            'pageSizeParam' => false
        ]);
        
        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->with('tags')
            ->orderBy(['dateCreated' => SORT_DESC])
            ->all();
        
        return $this->render('index', [
            'posts' => $models,
            'pagination' => $pagination,
        ]);
    }

    public function actionTag($tag)
    {
        $tag = Tag::findOne(['name' => $tag]);
        if (is_null($tag)) {
            throw new NotFoundHttpException("Tag not found");
        }

        $query = Post::findByTag($tag)->andWhere(['hidden' => false]);
        $countQuery = clone $query;

        $pagination = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 30,
            'pageSizeParam' => false
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->orderBy(['dateCreated' => SORT_DESC])
            ->all();

        return $this->render('tag', [
            'tag' => $tag,
            'posts' => $models,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
