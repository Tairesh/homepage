<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use Zelenin\yii\extensions\Rss\RssView;
use Zelenin\Feed;
use app\models\Post;

class RssController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->where(['hidden' => false])->orderBy(['dateCreated' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->set('Content-Type', 'application/rss+xml; charset=utf-8');

        return RssView::widget([
            'dataProvider' => $dataProvider,
            'channel' => [
                'title' => function ($widget, Feed $feed) {
                    $feed->addChannelTitle(Yii::$app->name);
                },
                'link' => "https://tairesh.xyz",
                'description' => 'Posts on tairesh.xyz',
                'language' => function ($widget, Feed $feed) {
                    return Yii::$app->language;
                },
                'image'=> function ($widget, Feed $feed) {
                    $feed->addChannelImage('https://tairesh.xyz/img/astronaut-icon-128.jpg', 'https://tairesh.xyz', 128, 128, 'Astronaut icon');
                },
            ],
            'items' => [
                'title' => function ($model, $widget, Feed $feed) {
                    return $model->title;
                },
                'description' => function ($model, $widget, Feed $feed) {
                    return "<![CDATA[{$model->content}]]";
                },
                'link' => function ($model, $widget, Feed $feed) {
                    return Url::toRoute(['post/view', 'slug' => $model->slug], true);
                },
                'author' => function ($model, $widget, Feed $feed) {
                    return "tairesh.rus@gmail.com (Ilya Agafonov)";
                },
                'guid' => function ($model, $widget, Feed $feed) {
                    return Url::toRoute(['post/view', 'slug' => $model->slug], true);
                },
                'pubDate' => function ($model, $widget, Feed $feed) {
                    return date(DATE_RSS, $model->dateCreated);
                },
            ]
        ]);
    }

}
