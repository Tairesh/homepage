
(function(){

    function updateGrayText() {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', '/motto/random');
        xhr.onload = function() {
            let result;
            if (xhr.status === 200) {
                result = xhr.responseText;
            } else {
                result = "Error " + xhr.status;
            }
            if (result !== document.getElementById('motto').innerHTML) {
                document.getElementById('motto').innerHTML = result;
            } else {
                updateGrayText();
            }
        };
        xhr.send();
    }
    document.getElementById('motto').onclick = updateGrayText;
})();
