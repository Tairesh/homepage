
(function() {

    function youtubeOnClick() {
        let iframe = document.createElement("iframe");
        iframe.setAttribute("src", "https://www.youtube.com/embed/"+ this.dataset.id + "?autoplay=1");
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        this.parentNode.replaceChild(iframe, this);
    }

    let div, n,
        v = document.getElementsByClassName("youtube-player");
    for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].dataset.id);
        div.innerHTML = '<img alt="video" src="https://i.ytimg.com/vi/' + v[n].dataset.id + '/hqdefault.jpg"><div class="play"></div>';
        div.onclick = youtubeOnClick;
        v[n].appendChild(div);
    }
})();
