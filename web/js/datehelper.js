(function(){

    if (navigator.cookieEnabled)
        document.cookie = "tzo="+ (new Date().getTimezoneOffset()) + "; path=/; expires=1year";

    function pad_zero(number) {
        return ((number < 10) ? '0' : '') + number;
    }

    function formatDate(timestamp) {
        let date = new Date(timestamp*1000),
            day = date.getDate(),
            month = date.getMonth()+1,
            year = date.getFullYear(),
            hours = date.getHours(),
            minutes =  date.getMinutes();
        return pad_zero(day) + '.' + pad_zero(month) + '.' + year + ' ' +
               pad_zero(hours) + ':' + pad_zero(minutes);
    }

    let elements = document.getElementsByClassName("date");
    for (let i=0; i < elements.length; i++) {
        let el = elements[i],
            timestamp = parseInt(el.dataset['timestamp']);
        el.textContent = formatDate(timestamp);
    }

})();
