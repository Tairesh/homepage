<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mottos}}`.
 */
class m191219_060826_create_mottos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mottos}}', [
            'id' => $this->primaryKey(),
            'text' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mottos}}');
    }
}
