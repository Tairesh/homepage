<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pages}}`.
 */
class m191219_032748_create_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'label' => $this->string(127)->null()->defaultValue(NULL),
            'slug' => $this->string(255)->notNull()->unique(),
            'content' => $this->text()->notNull(),
            'isActive' => $this->boolean(),
            'inMenu' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }
}
