<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posts2tags}}`.
 */
class m191219_042350_create_posts2tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%posts2tags}}', [
            'postId' => $this->integer()->unsigned(),
            'tagId' => $this->integer()->unsigned(),
        ]);
        $this->createIndex('{{%posts2tagsUnique}}', '{{%posts2tags}}', ['tagId', 'postId'], true);
        $this->addForeignKey('{{%posts2tagsPostId}}', '{{%posts2tags}}',
            'postId', '{{%posts}}', 'id');
        $this->addForeignKey('{{%posts2tagsTagId}}', '{{%posts2tags}}',
            'tagId', '{{%tags}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%posts2tagsTagId}}', '{{%posts2tags}}');
        $this->dropForeignKey('{{%posts2tagsPostId}}', '{{%posts2tags}}');
        $this->dropIndex('{{%posts2tagsUnique}}', '{{%posts2tags}}');
        $this->dropTable('{{%posts2tags}}');
    }
}
