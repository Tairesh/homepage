<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%column_preview_on_posts}}`.
 */
class m191225_055655_drop_column_preview_on_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn("{{%posts}}", 'preview');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn("{{%posts}}", 'preview', $this->text()->notNull()->defaultValue(''));
    }
}
