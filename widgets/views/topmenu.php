<?php

use app\models\Page;
use yii\helpers\Html;

/** @var $pages Page[] */

?>
<ul class="topmenu">
    <?php foreach ($pages as $page): ?>
        <li>
            <?= Html::a($page->label, "/{$page->slug}",
                ["class" => "abtn".(Yii::$app->request->url == "/{$page->slug}" ? ' active' : '')]) ?>
        </li>
    <?php endforeach ?>
</ul>
