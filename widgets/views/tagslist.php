<?php

/** @var $tags \app\models\Tag[] */

use yii\helpers\Html;


?>
<ul class="tagslist">
    <?php foreach ($tags as $tag): ?>
        <li>
            <?= Html::a("{$tag->name} [{$tag->rating}]", "/tags/{$tag->name}",
                ['class'=>Yii::$app->request->url == "/tags/{$tag->name}" ? 'active' : '']) ?>
        </li>
    <?php endforeach ?>
</ul>