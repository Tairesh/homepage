<?php


namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Page;


class TopMenu extends Widget
{

    /**
     * @var Page[] $pages
     */
    public array $pages;

    public function init()
    {
        $this->pages = Page::find()->where(['inMenu' => true])->all();
        parent::init();
    }

    public function run()
    {
        return $this->render('topmenu', [
            'pages' => $this->pages,
        ]);
    }

}