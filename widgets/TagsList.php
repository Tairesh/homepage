<?php


namespace app\widgets;


use yii\base\Widget;
use app\models\Tag;
use app\models\Post;


class TagsList extends Widget
{

    public static $allTags = NULL;
    public ?Post $post = NULL;


    public function init()
    {
        if (is_null(static::$allTags)) {
            static::$allTags = Tag::find()->where(['>', 'rating', 0])->orderBy(['name' => 'ASC'])->all();
        }
        parent::init();
    }

    public function run()
    {
        return $this->render('tagslist', [
            'tags' => is_null($this->post) ? static::$allTags : $this->post->tags,
        ]);
    }

}