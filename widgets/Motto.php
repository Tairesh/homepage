<?php


namespace app\widgets;

use app\models\Motto as MottoModel;
use yii\base\Widget;


class Motto extends Widget
{

    public string $text;

    public function init()
    {
        $this->text = MottoModel::randomText();
        parent::init();
    }

    public function run()
    {
        return $this->render('graytext', [
            'text' => $this->text,
        ]);
    }

}