<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;

class CompressController extends Controller
{

    const WEBROOT = __DIR__ . '/../web';
    const CSS_PATH = self::WEBROOT . '/css/bundle.min.css';
    const JS_PATH = self::WEBROOT . '/js/bundle.min.js';

    const CSS_FILES = [
        self::WEBROOT . '/css/site.css',
        self::WEBROOT . '/css/rainbow-github.css',
        self::WEBROOT . '/css/youtube.css',
    ];

    const JS_FILES = [
        self::WEBROOT . '/js/rainbow.min.js',
        self::WEBROOT . '/js/datehelper.js',
        self::WEBROOT . '/js/youtube.js',
        self::WEBROOT . '/js/motto-updater.js',
    ];

    public function actionIndex()
    {
        Yii::setAlias('@webroot', self::WEBROOT);
        Yii::setAlias('@web', '/');
        $minifier = new CSS();
        foreach (self::CSS_FILES as $path) {
            $minifier->add($path);
        }
        $minifier->minify(self::CSS_PATH);

        $minifier = new JS();
        foreach (self::JS_FILES as $path) {
            $minifier->add($path);
        }
        $minifier->minify(self::JS_PATH);
    }
}
