<?php

namespace app\helpers;

use Yii;


class DateHelper
{
    public static function getTimeAgo($time): string
    {

        $time_difference = time() - $time;

        if( $time_difference < 1 ) { return 'less than 1 second ago'; }
        $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
            30 * 24 * 60 * 60       =>  'month',
            24 * 60 * 60            =>  'day',
            60 * 60                 =>  'hour',
            60                      =>  'minute',
            1                       =>  'second'
        );

        foreach( $condition as $secs => $str )
        {
            $d = $time_difference / $secs;

            if( $d >= 1 )
            {
                $t = round( $d );
                return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
            }
        }
        return 'eternity ago';
    }

    public static function dateInClientTimezone($format, int $timestamp)
    {
        $tzo = isset($_COOKIE['tzo']) ? (int)$_COOKIE['tzo'] : 0;
        return date($format, $timestamp - $tzo*60);
    }

}
